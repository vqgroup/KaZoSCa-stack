base:
  "*":
    - etchostname
    - openssh
    - docker
    - cassandra
    - zookeeper
    - spark
    - nftables
    - telegraf
  "salt":
    - tickstack
    - experiment_data
